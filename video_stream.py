import cv2
import numpy as np
import os

data_directory = '/home/matthew/data/dolphins'
video_files = os.listdir(data_directory)
file = '{}/{}'.format(data_directory, video_files[1])

class VideoStream:
	"""
	Pythonic interface for OpenCV video capture objects. Implements
	indexing and iteration for reading through a stored video file.
	"""
	def __init__(self, file:str, bw:bool=False):
		"""
		Pass location of video file to the constructor.
		"""
		self.cap = cv2.VideoCapture(file)
		self.length = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
		self.width = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
		self.height = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
		self.fps = self.cap.get(cv2.CAP_PROP_FPS)
		self.index = 0 
		self.bw = bw


	def __len__(self):
		"""
		Number of frames in video.
		"""
		return self.length


	def __getitem__(self, index:int):
		"""
		Returns index^th frame of video. Also supports slicing.
		"""
		if isinstance(index, slice):
			indices = range(*index.indices(len(self)))
			return [self._get_frame(i) for i in indices]
		else: 
			return self._get_frame(index)


	def __next__(self):
		if self.index > self.length:
			raise StopIteration 
		else:
			self.index += 1
			return self.__getitem__(self.index-1)


	def __iter__(self):
		"""
		Video can be iterated through with for and while. The RGB or BW values of the
		frame are returned depending on if self.bw is set to to True or False 
		(default value is False).
		"""
		return self


	def _get_frame(self, index:bool):
		self.cap.set(cv2.CAP_PROP_POS_FRAMES, index)
		frame = self.cap.read()[1]
		if self.bw: return cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		else: return frame


	def convert_to_bw(self, bw:bool):
		"""
		Pass True to this function if you want __getitem__ to return b+w pixels.
		"""
		self.bw = bw


	def get_resolution(self):
		return (self.width, self.height)


	def get_fps(self):
		return self.fps


	def play_video(self, bw:bool=False):
		if bw:
			self.convert_to_bw(bw)

		try:
			for frame in self:
				cv2.imshow('frame', frame)
				cv2.waitKey(1)

		finally:
			cv2.destroyAllWindows()

video_stream = VideoStream(file, True)
#video_stream.play_video(bw=True)

frames = []
for i, frame in enumerate(video_stream):
	frames.append(frame)
	if i == 100:
		break